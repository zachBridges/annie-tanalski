<div class="interactive-modal-content text-center js-giving2">
    <div class="row">
      <div class="column small-12">
        <div class="interactive-modal-image">
          <img
            src="./public/images/interactive/giving2/giving1.jpg"
            class="w100"
          />
        </div>
        <div class="show-for-medium-up"><br /><br /><br /></div>
      </div>
    </div>
    <div class="row">
      <div class="column small-12 medium-6">
        <div class="interactive-modal-image">
          <img src="./public/images/interactive/giving2/giving2.jpg" />
        </div>
        <div class="interactive-modal-image">
          <img src="./public/images/interactive/giving2/giving3.jpg" />
        </div>
      </div>
      <div class="column small-12 medium-6">
        <div class="interactive-modal-image">
          <img src="./public/images/interactive/giving2/giving4.jpg" />
        </div>
        <div class="interactive-modal-image">
          <img src="./public/images/interactive/giving2/giving5.jpg" />
        </div>       
      </div>
    </div>
    <div class="row">
      <div class="column  small-12 small-centered">
        <div class="interactive-modal-image">
          <img src="./public/images/interactive/giving2/giving6.jpg" />
        </div>
      </div>
    </div>    
    <div class="row">
      <div class="column  small-12 small-centered">
        
        <div class="interactive-modal-image">
          <img src="./public/images/interactive/giving2/giving7.jpg" />
        </div>
      </div>
    </div>
    <div class="row">
      <div class="column small-12">
        <div class="interactive-modal-title text-right">
          <p class="brand">AARP holiday thrive guide</p>
          <p class="promo">sweepstakes engagement hub</p>
          <p>2018</p>
        </div>
        <a class="close-reveal-modal mod-inline" aria-label="Close">Close</a>
      </div>
    </div>
  </div>