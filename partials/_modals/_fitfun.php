<div class="interactive-modal-content text-center js-fitfun">
    <div class="row">
      <div class="column small-12">
        <div class="interactive-modal-image">
          <img
            src="./public/images/interactive/fitfun/fitfun1.jpg"
            class="w100"
          />
        </div>
        <div class="show-for-medium-up"><br /><br /><br /></div>
      </div>
    </div>
    <div class="row">
      <div class="column small-12 medium-6">
        <div class="interactive-modal-image">
          <img src="./public/images/interactive/fitfun/fitfun2.jpg" />
        </div>
        <div class="interactive-modal-image">
          <img src="./public/images/interactive/fitfun/fitfun3.jpg" />
        </div>
      </div>
      <div class="column small-12 medium-6">
        <div class="interactive-modal-image">
          <img src="./public/images/interactive/fitfun/fitfun4.jpg" />
        </div>
        <div class="interactive-modal-image">
          <img src="./public/images/interactive/fitfun/fitfun5.jpg" />
        </div>
      </div>
    </div>
    <div class="row">
      <div class="column  small-12 small-centered">
        <div class="interactive-modal-image">
          <img src="./public/images/interactive/fitfun/fitfun6.jpg" />
        </div>
      </div>
    </div>
    <div class="row">
      <div class="column small-12">
        <div class="interactive-modal-title text-right">
          <p class="brand">AARP FIT & FUN Health Challenge</p>
          <p class="promo">sweepstakes engagement hub</p>
          <p>2019</p>
        </div>
        <a class="close-reveal-modal mod-inline" aria-label="Close">Close</a>
      </div>
    </div>
  </div>