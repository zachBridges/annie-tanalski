<section class="content-section">
  <label class="content-section-title show-for-medium-up mod-design">
    Design
  </label>

  <div class="js-slider-design content-slider-design">
    <div class="">
      <a href="#" data-reveal-id="designModal" data-reveal-design>
        <img data-lazy="./public/images/design/design1.jpg"/>
      </a>
    </div>
    <div class="">
      <a href="#" data-reveal-id="designModal" data-reveal-design>
        <img data-lazy="./public/images/design/design2.jpg"/>
      </a>
    </div>
    <div class="">
      <a href="#" data-reveal-id="designModal" data-reveal-design>
        <img data-lazy="./public/images/design/design3.jpg"/>
      </a>
    </div>
    <div class="">
      <a href="#" data-reveal-id="designModal" data-reveal-design>
        <img data-lazy="./public/images/design/design4.jpg"/>
      </a>
    </div>
    <div class="">
      <a href="#" data-reveal-id="designModal" data-reveal-design>
        <img data-lazy="./public/images/design/design5.jpg"/>
      </a>
    </div>
    <div class="">
      <a href="#" data-reveal-id="designModal" data-reveal-design>
        <img data-lazy="./public/images/design/design6.jpg"/>
      </a>
    </div>
    <div class="">
      <a href="#" data-reveal-id="designModal" data-reveal-design>
        <img data-lazy="./public/images/design/design7.jpg"/>
      </a>
    </div>
    <div class="">
      <a href="#" data-reveal-id="designModal" data-reveal-design>
        <img data-lazy="./public/images/design/design8.jpg"/>
      </a>
    </div>
    <div class="">
      <a href="#" data-reveal-id="designModal" data-reveal-design>
        <img data-lazy="./public/images/design/design9.jpg"/>
      </a>
    </div>
    <div class="">
      <a href="#" data-reveal-id="designModal" data-reveal-design>
        <img data-lazy="./public/images/design/design10.jpg"/>
      </a>
    </div>
    <div class="">
      <a href="#" data-reveal-id="designModal" data-reveal-design>
        <img data-lazy="./public/images/design/design11.jpg"/>
      </a>
    </div>
  </div>

</section>
