
<footer class="footer-mobile">
  <div class="row">
    <div class="column small-12 medium-6 text-center show-for-small-only">
      <h4><a href="mailto:atanalski@gmail.com">atanalski@gmail.com</a></h4>
    </div>  	
    <div class="column small-12 medium-6 text-center show-for-small-only">
      <h4><a href="http://www.annietanalskidesign.com/resume/annie-tanalski-resume.pdf" target="_blank">Resume</a></h4>
    </div>     
    <div class="column small-12 medium-6 small-text-center medium-text-left">
      <h4>&copy; Annie Tanalski</h4>
    </div>
    <div class="column small-12 medium-6 medium-text-right show-for-medium-up">
      <h4><a href="mailto:atanalski@gmail.com">atanalski@gmail.com</a></h4>
      <h4><a href="http://www.annietanalskidesign.com/resume/annie-tanalski-resume.pdf" target="_blank">Resume</a></h4>

    </div>
  </div>
</footer>