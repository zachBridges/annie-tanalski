<section class="content-section">
  <label class="content-section-title show-for-medium-up mod-interactive">
    interactive
  </label>

  <div class="js-slider-interactive content-slider-interactive">
    <div class="">
      <a href="#" data-reveal-id="interactiveModal" data-reveal-interactive="fitfun">
        <img data-lazy="./public/images/interactive/fitfun/fitfun1.jpg"/>
      </a>
    </div>    
    <div class="">
      <a href="#" data-reveal-id="interactiveModal" data-reveal-interactive="company">
        <img data-lazy="./public/images/interactive/company/company0.jpg"/>
      </a>
    </div>      
    <div class="">
      <a href="#" data-reveal-id="interactiveModal" data-reveal-interactive="jbeam">
        <img data-lazy="./public/images/interactive/jbeam/jbeam-thumb.jpg"/>
      </a>
    </div>    
    <div class="">
      <a href="#" data-reveal-id="interactiveModal" data-reveal-interactive="giving2">
        <img data-lazy="./public/images/interactive/giving2/giving1.jpg"/>
      </a>
    </div>        
    <div class="">
      <a href="#" data-reveal-id="interactiveModal" data-reveal-interactive="declare">
        <img data-lazy="./public/images/interactive/declare/declare1.jpg"/>
      </a>
    </div>    
    <div class="">
      <a href="#" data-reveal-id="interactiveModal" data-reveal-interactive="bobbi">
        <img data-lazy="./public/images/interactive/bobbi/bobbi1.jpg"/>
      </a>
    </div>    
    <div class="">
      <a href="#" data-reveal-id="interactiveModal" data-reveal-interactive="retirement">
        <img data-lazy="./public/images/interactive/retirement/retirement-thumb.jpg"/>
      </a>
    </div>     
    <div class="">
      <a href="#" data-reveal-id="interactiveModal" data-reveal-interactive="giving">
        <img data-lazy="./public/images/interactive/giving/giving1.jpg"/>
      </a>
    </div>            
    <div class="">
      <a href="#" data-reveal-id="interactiveModal" data-reveal-interactive="royal">
        <img data-lazy="./public/images/interactive/royal/royal1.jpg"/>
      </a>
    </div>
    <div class="">
      <a href="#" data-reveal-id="interactiveModal" data-reveal-interactive="bebe">
        <img data-lazy="./public/images/interactive/bebe/bebe1.jpg"/>
      </a>
    </div>    
    <div class="">
      <a href="#" data-reveal-id="interactiveModal" data-reveal-interactive="jedi">
        <img data-lazy="./public/images/interactive/jedi/jedi1.jpg"/>
      </a>
    </div>
  </div>
</section>