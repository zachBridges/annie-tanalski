<header>
  <div class="show-for-medium-up nav-micro">
    <div class="row collapse">
      <div class="column medium-4 large-6">
        <div class="nav-micro-title"><a href="./">Annie Tanalski</a></div>
      </div>
      <div class="column medium-8 large-6 text-right">
        <ul class="nav-micro-controls">
         <li class="nav-micro-controls-item mod-illustration" data-navhover="illustration"><a href="#section_illustration">Illustration</a></li>
         <li class="nav-micro-controls-item mod-design"  data-navhover="design"><a href="#section_design">Design</a></li>
         <li class="nav-micro-controls-item mod-interactive"  data-navhover="interactive"><a href="#section_interactive">Interactive</a></li>
         <li class="nav-micro-controls-item mod-contact"  data-navhover="contact"><a href="#section_contact">Contact</a></li>
          </a>
        </ul>
      </div>
    </div>
  </div>

  <div class="brain show-for-medium-up">
    <div class="brain-container">
      <div class="brain-image">
        <a href="#section_contact" class="js-mobile-goto-contact-fromdrawer">
          <div class="brain-contact">
            <div class="brain-contact-image">
              <img src="public/images/brain/contact.png" />
            </div>
            <div class="brain-contact-label">
              <img src="public/images/brain/contact-label.png" class="brain-label-md-up"/>
              <img src="public/images/brain/contact-label-sm.png" class="brain-label-sm-up" />
            </div>
          </div>
        </a>
        <a href="#section_design"  data-brain-showsection="design">
          <div class="brain-design">
            <div class="brain-design-image">
              <img src="public/images/brain/design.png" />
            </div>
            <div class="brain-design-label">
              <img src="public/images/brain/design-label.png" />
            </div>
          </div>
        </a>
        <a href="#section_illustration" data-brain-showsection="illustration">
          <div class="brain-illustration">
            <div class="brain-illustration-image">
              <img src="public/images/brain/illustration.png" />
            </div>
            <div class="brain-illustration-label">
              <img src="public/images/brain/illustration-label.png" />
            </div>
          </div>
        </a>
        <a href="#section_interactive" data-brain-showsection="interactive">
          <div class="brain-interactive">
            <div class="brain-interactive-image">
              <img src="public/images/brain/interactive.png" />
            </div>
            <div class="brain-interactive-label">
              <img src="public/images/brain/interactive-label.png" class="brain-label-md-up"/>
              <img src="public/images/brain/interactive-label-sm.png" class="brain-label-sm-up" />
            </div>
          </div>
        </a>
      </div>
    </div> <!--/.brain-container-->
  </div><!--/.brain-->

  <div class="brain-mobile show-for-small-only">
    <img src="public/images/brain/brain-mobile.png"/>
  </div>
</header>
