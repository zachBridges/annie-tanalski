<div id="section_illustration" class="show-for-medium-up">
  <?php require 'partials/_illustration-slider.php';?>
</div>

<div id="section_design" class="show-for-medium-up">
  <?php require 'partials/_design-slider.php';?>
</div>

<div id="section_interactive" class="show-for-medium-up">
  <?php require 'partials/_interactive-slider.php';?>
</div>

<div id="section_illustration_mobile" class="mobile-content-section mod-illustration js-mobile-section-cta">
  <div class="mobile-content-section-overlay"></div>
  <label class="mobile-content-section-title">Illustration</label>
</div>
<div id="section_design_mobile" class="mobile-content-section mod-design js-mobile-section-cta">
  <div class="mobile-content-section-overlay"></div>
  <label class="mobile-content-section-title">Design</label>
</div>
<div id="section_interactive_mobile" class="mobile-content-section mod-interactive js-mobile-section-cta">
  <div class="mobile-content-section-overlay"></div>
  <label class="mobile-content-section-title">Interactive</label>
</div>

<div class="mobile-content-body mod-illustration">
  <?php require 'partials/_mobile_illustration_slideshow.php';?>
</div>

<div class="mobile-content-body mod-design">
  <?php require 'partials/_mobile_design_slideshow.php';?>
</div>

<div class="mobile-content-body mod-interactive">
  <?php require 'partials/_mobile_interactive_slideshow.php';?>
</div>

<div id="section_contact">
  <?php require 'partials/_contact.php';?>
</div>


