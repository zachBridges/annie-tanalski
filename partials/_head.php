<meta charset="utf-8"> 

<title>The Portfolio of Annie Tanalski</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta name="description" content="">
<meta name="keywords" content="">
<link rel="canonical" href="//www.annietanalskidesign.com" />

<meta property="og:title" content="" />
<meta property="og:url" content=" />
<meta property="og:description" content="" />
<meta property="og:image" content="" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website"/>

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@" />
<meta name="twitter:title" content="" />
<meta name="twitter:description" content="" />
<meta name="twitter:image" content="/>

<link rel="shortcut icon" href="./favicon.ico" type="image/x-icon" />

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href="//fonts.googleapis.com/css?family=Open+Sans|Raleway:300" rel="stylesheet">
<link rel="stylesheet" href="public/css/normalize.css">
<link rel="stylesheet" href="public/css/foundation.min.css">
<link rel="stylesheet" href="public/css/app.min.css">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

<script type="text/javascript" src="public/js/vendor/js.cookie.min.js"></script>
<script type="text/javascript" src="public/js/vendor/jquery.js"></script>
<script type="text/javascript" src="public/js/vendor/typed.min.js"></script>
<script type="text/javascript" src="public/js/foundation/foundation.js"></script>
<script type="text/javascript" src="public/js/foundation/foundation.equalizer.js"></script>
<script type="text/javascript" src="public/js/foundation/foundation.offcanvas.js"></script>
<script type="text/javascript" src="public/js/foundation/foundation.reveal.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>


<script type="text/javascript" src="public/js/app.js"></script>