<div id="illustrationModal" class="reveal-modal illustration-modal" data-reveal aria-hidden="true" role="dialog">
  <div class="illustration-target text-center">
   <img src=""></a>

  </div>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>

<div id="designModal" class="reveal-modal design-modal" data-reveal aria-hidden="true" role="dialog">
  <div class="design-target text-center">
   <img src=""></a>

  </div>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>

<div id="interactiveModal" class="reveal-modal interactive-modal" data-reveal aria-hidden="true" role="dialog">

  <?php require 'partials/_modals/_fitfun.php';?>

  <?php require 'partials/_modals/_giving2.php';?>

  <div class="interactive-modal-content text-center js-bobbi">
    <div class="row"><div class="column small-12">
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/bobbi/bobbi1.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/bobbi/bobbi2.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/bobbi/bobbi3.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/bobbi/bobbi4.jpg"/>
    </div>
    <div class="interactive-modal-title text-right">
      <p class="brand">Bobbi Brown Get Framed Giveaway</p>
      <p class="promo">eyewear selector sweeps</p>
      <p>2014</p>
    </div>
    <a class="close-reveal-modal mod-inline" aria-label="Close">Close</a>
    </div></div>
  </div>

  <div class="interactive-modal-content text-center js-company">
    <div class="row"><div class="column small-12">
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/company/company1.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/company/company2.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/company/company3.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/company/company4.jpg"/>
    </div>
    <div class="interactive-modal-title text-right">
      <p class="brand">AARP in good company</p>
      <p class="promo">Engagement hub</p>
      <p>2018</p>
    </div>
    <a class="close-reveal-modal mod-inline" aria-label="Close">Close</a>
    </div></div>
  </div>

  <div class="interactive-modal-content text-center js-royal">
    <div class="row"><div class="column small-12">
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/royal/royal1.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/royal/royal2.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/royal/royal3.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/royal/royal4.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/royal/royal5.jpg"/>
    </div>    
    <div class="interactive-modal-title text-right">
      <p class="brand">Royal caribbean© TICKET TO ADVENTURE</p>
      <p class="promo">Engagement hub</p>
      <p>2017</p>
    </div>
    <a class="close-reveal-modal mod-inline" aria-label="Close">Close</a>
    </div></div>
  </div>

  <div class="interactive-modal-content text-center js-retirement">
    <div class="row"><div class="column small-12">
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/retirement/retirement1.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/retirement/retirement2.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/retirement/retirement3.jpg"/>
    </div>
    <div class="interactive-modal-title text-right">
      <p class="brand">AAARP Retirement in reach</p>
      <p class="promo">Engagement Hub</p>
      <p class="year">2017</p>
    </div>
    <a class="close-reveal-modal mod-inline" aria-label="Close">Close</a>
    </div></div>
  </div>


  <div class="interactive-modal-content text-center js-giving">
    <div class="row"><div class="column small-12">
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/giving/giving1.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/giving/giving2.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/giving/giving3.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/giving/giving4.jpg"/>
    </div>    
    <div class="interactive-modal-title text-right">
      <p class="brand">AARP small gestures, big impact</p>
      <p class="promo">Engagement Hub</p>
      <p class="year">2017</p>
    </div>
    <a class="close-reveal-modal mod-inline" aria-label="Close">Close</a>
    </div></div>
  </div>


  <div class="interactive-modal-content text-center js-declare">
    <div class="row"><div class="column small-12">
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/declare/declare1.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/declare/declare2.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/declare/declare3.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/declare/declare4.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/declare/declare5.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/declare/declare6.jpg"/>
    </div>
    <div class="interactive-modal-title text-right">
      <p class="brand">AARP Declare a day</p>
      <p class="promo">Engagement Hub</p>
      <p class="year">2017</p>
    </div>
    <a class="close-reveal-modal mod-inline" aria-label="Close">Close</a>
    </div></div>
  </div>

  <div class="interactive-modal-content text-center js-bebe">
    <div class="row"><div class="column small-12">
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/bebe/bebe1.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/bebe/bebe2.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/bebe/bebe3.jpg"/>
    </div>
    <div class="interactive-modal-title text-right">
      <p class="brand">BEBE® WORKDAY TO SOIRÉE</p>
      <p class="promo">daily prize calendar</p>
      <p class="year">2014</p>
    </div>
    <a class="close-reveal-modal mod-inline" aria-label="Close">Close</a>
    </div></div>
  </div>

  <div class="interactive-modal-content text-center js-jedi">
    <div class="row"><div class="column small-12">
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/jedi/jedi1.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/jedi/jedi2.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/jedi/jedi3.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/jedi/jedi4.jpg"/>
    </div>
    <div class="interactive-modal-image">
      <img src="./public/images/interactive/jedi/jedi5.jpg"/>
    </div>
    <div class="interactive-modal-title text-right">
      <p class="brand">HASBRO® JOIN THE JEDI</p>
      <p class="promo">Engagement Hub</p>
      <p class="year">2011</p>
    </div>
    <a class="close-reveal-modal mod-inline" aria-label="Close">Close</a>
    </div></div>
  </div>

  <div class="interactive-modal-content text-center js-jbeam">
    <div class="row">
      <div class="column small-12">
        <div class="interactive-modal-image">
          <img src="./public/images/interactive/jbeam/jbeam-thumb.jpg"/>
        </div>   
      </div>
    </div>
    <div class="row">
      <div class="column small-12 medium-6">     
        <div class="interactive-modal-image">
          <img src="./public/images/interactive/jbeam/jbeam1.jpg"/>
        </div>
        <div class="interactive-modal-image">
          <img src="./public/images/interactive/jbeam/jbeam2.jpg"/>
        </div>
        <div class="interactive-modal-image">
          <img src="./public/images/interactive/jbeam/jbeam3.jpg"/>
        </div>
      </div>
      <div class="column small-12 medium-6">
        <div class="interactive-modal-image">
          <img src="./public/images/interactive/jbeam/jbeam4.jpg"/>
        </div>
        <div class="interactive-modal-image">
          <img src="./public/images/interactive/jbeam/jbeam5.jpg"/>
        </div>
        <div class="interactive-modal-image">
          <img src="./public/images/interactive/jbeam/jbeam6.jpg"/>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="column small-12">
        <div class="interactive-modal-image">
          <img src="./public/images/interactive/jbeam/jbeam7.jpg"/>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="column small-12">
        <div class="interactive-modal-title text-right">
          <p class="brand">Jim Beam© GO INSIDE THE WORLD OF BEAM©</p>
          <p class="promo">bourbon education microsite</p>
          <p>2016–2017</p>
        </div>
        <a class="close-reveal-modal mod-inline" aria-label="Close">Close</a>
      </div>
    </div>

  </div>

  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
