<section class="content-section content-section-contact show-for-medium-up">
  <div class="content-section-contact-body">
    <div class="row">
      <div class="column small-12 medium-8 small-only-text-center">
        <div class="content-section-contact-headline">
          <h2>Contact</h2>
        </div>
      </div>
      <div class="column small-6 medium-2">

      </div>
      <div class="column small-6 medium-2">
        <div class="content-section-contact-icon text-center small-only-text-right">
          <a href="mailto:atanalski@gmail.com">
            <img src="./public/images/icon-mail.png" /></a>
        </div>
      </div>

    </div>
  </div>
</section>
