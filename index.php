<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="" lang="en" >

<head>
  <?php require 'partials/_head.php';?>
</head>

<body>

  <section class="tab-bar-section show-for-small-only">
    <div class="mobile-header">
      <div class="annie-title-back-container">
        <span class="js-nav-back annie-title-back">&laquo; back</span>
      </div>
      <a href="./"><h1 class="annie-title">Annie Tanalski</h1></a>
    </div>
  </section>

  <div class="off-canvas-wrap container" data-offcanvas>
    <div class="inner-wrap">

      <nav class="tab-bar show-for-small-only annie-mobilenav">

        <section class="right-small">
          <a class="right-off-canvas-toggle menu-icon annie-mobilenav-icon" href="#">
            <span class="annie-mobilenav-dot"></span>
            <span class="annie-mobilenav-dot"></span>
            <span class="annie-mobilenav-dot"></span>
          </a>
        </section>
      </nav>

      <aside class="right-off-canvas-menu annie-mobilenav-menu">
        <ul class="off-canvas-list">
          <li class="js-mobile-drawer-home"><a href="#">Home</a></li>
          <li data-goto-mobile-section="illustration"><a href="#">Illustration</a></li>
          <li data-goto-mobile-section="design"><a href="#">Design</a></li>
          <li data-goto-mobile-section="interactive"><a href="#">Interactive</a></li>
          <li class="js-mobile-goto-contact"><a href="#">Contact</a></li>
        </ul>
      </aside>

      <section class="content-container">
        <?php require 'partials/_header.php';?>

        <?php require 'partials/_content.php';?>

        <?php require 'partials/_footer.php';?>
      </section>

    <a class="exit-off-canvas annie-off-canvas"></a>

    </div>
  </div>

  <?php require 'partials/_modals.php';?>

</body>

</html>
