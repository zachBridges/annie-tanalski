var gulp = require('gulp'),
  plumber = require('gulp-plumber'),
  rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var minifycss = require('gulp-minify-css');
var sass = require('gulp-sass');
var gutil = require('gulp-util');

gulp.task('default', function () {
  gulp.watch("public/scss/**/*.scss", ['styles']);
});

gulp.task('styles', function () {
  gulp.src(['public/scss/app.scss'])
    // .pipe(plumber({
    //   errorHandler: function (error) {
    //     console.log(error);
    //     console.log(error.message);
    //     gutil.beep();
    //     this.emit('end');
    //   }
    // }))
    .pipe(sass({
      // debugInfo: true,
      lineNumbers: true
    }))
    .pipe(autoprefixer('last 2 version'))
    .pipe(minifycss())
    // .pipe(gulp.dest('public/css'))
    .pipe(rename({ suffix: '.min' }))

    .pipe(gulp.dest('public/css'))
});
