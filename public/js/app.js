$(function(){

  function doBrainHighlight(){
    setTimeout(function(){
      $('.nav-micro-controls-item.mod-illustration').addClass('highlight');
      $('.brain-illustration').addClass('show');
    },500);
    setTimeout(function(){
      $('.nav-micro-controls-item.mod-illustration').removeClass('highlight');
      $('.brain-illustration').removeClass('show');
    },1500);
    setTimeout(function(){
      $('.nav-micro-controls-item.mod-design').addClass('highlight');
      $('.brain-design').addClass('show');
    },1600);
    setTimeout(function(){
      $('.nav-micro-controls-item.mod-design').removeClass('highlight');
      $('.brain-design').removeClass('show');
    },2600);
    setTimeout(function(){
      $('.nav-micro-controls-item.mod-interactive').addClass('highlight');
      $('.brain-interactive').addClass('show');
    },2800);
    setTimeout(function(){
      $('.nav-micro-controls-item.mod-interactive').removeClass('highlight');
      $('.brain-interactive').removeClass('show');
    },3800);
    setTimeout(function(){
      $('.nav-micro-controls-item.mod-contact').addClass('highlight');
      $('.brain-contact').addClass('show');
    },3900);
    setTimeout(function(){
      $('.nav-micro-controls-item.mod-contact').removeClass('highlight');
      $('.brain-contact').removeClass('show');
    },4900);

    if(window.innerWidth <= 640){
      setTimeout(doBrainHighlight, 5900);
    }
  }


  setTimeout(doBrainHighlight, 1000);

  var querystring = get_query();

  var pageName = document.URL.toLowerCase();

  $(document).foundation(
  {
    equalizer : {
      // Specify if Equalizer should make elements equal height once they become stacked.
      equalize_on_stack: false,
      // Allow equalizer to resize hidden elements
      act_on_hidden_el: true
    },
  });

  var sliderButtonPrev = "<button class='slick-prev'><i class='fa fa-angle-left'></i></button>";
  var sliderButtonNext = "<button class='slick-next'><i class='fa fa-angle-right'></i></button>";


  $('.js-slider-illustration').slick({
    dots: false,
    autoplay: true,
    autoplaySpeed: 5500,
    infinite: false,
    speed: 300,
    variableWidth: true,
    lazyLoad: 'progressive',
    centerMode: false,
    swipeToSlide: true,
    placeholders: false,
    arrows: true,
    prevArrow: sliderButtonPrev,
    nextArrow: sliderButtonNext
    /*
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true

        }
      }
    ]
    */
  });

  $('.js-slider-illustration-mobile').slick({
    dots: false,
    autoplay: true,
    autoplaySpeed: 5500,
    infinite: false,
    speed: 300,
    variableWidth: true,
    prevArrow: false,
    nextArrow: false,
    lazyLoad: 'progressive',
    centerMode: false,
    swipeToSlide: true,
    placeholders: false,
    /*
     responsive: [
     {
     breakpoint: 1024,
     settings: {
     slidesToShow: 3,
     slidesToScroll: 3,
     infinite: true,
     dots: true

     }
     }
     ]
     */
  });

  $('.js-slider-design').slick({
    dots: false,
    autoplay: true,
    autoplaySpeed: 5500,
    infinite: false,
    speed: 300,
    variableWidth: true,
    lazyLoad: 'progressive',
    centerMode: false,
    swipeToSlide: true,
    arrows: true,
    prevArrow: sliderButtonPrev,
    nextArrow: sliderButtonNext
    /*
     responsive: [
     {
     breakpoint: 1024,
     settings: {
     slidesToShow: 3,
     slidesToScroll: 3,
     infinite: true,
     dots: true

     }
     }
     ]
     */
  });

  $('.js-slider-design-mobile').slick({
    dots: false,
    autoplay: true,
    autoplaySpeed: 5500,
    infinite: false,
    speed: 300,
    variableWidth: true,
    prevArrow: false,
    nextArrow: false,
    lazyLoad: 'progressive',
    centerMode: false,
    swipeToSlide: true,
    placeholders: false,
    /*
     responsive: [
     {
     breakpoint: 1024,
     settings: {
     slidesToShow: 3,
     slidesToScroll: 3,
     infinite: true,
     dots: true

     }
     }
     ]
     */
  });

  $('.js-slider-interactive').slick({
    dots: false,
    autoplay: true,
    autoplaySpeed: 5500,
    infinite: false,
    speed: 300,
    variableWidth: true,
    lazyLoad: 'progressive',
    centerMode: false,
    swipeToSlide: true,
    arrows: true,
    prevArrow: sliderButtonPrev,
    nextArrow: sliderButtonNext
    /*
     responsive: [
     {
     breakpoint: 1024,
     settings: {
     slidesToShow: 3,
     slidesToScroll: 3,
     infinite: true,
     dots: true

     }
     }
     ]
     */
  });

  $('.js-slider-interactive-mobile').slick({
    dots: false,
    autoplay: true,
    autoplaySpeed: 5500,
    infinite: false,
    speed: 300,
    variableWidth: true,
    prevArrow: false,
    nextArrow: false,
    lazyLoad: 'progressive',
    centerMode: false,
    swipeToSlide: true,
    placeholders: false,
    /*
     responsive: [
     {
     breakpoint: 1024,
     settings: {
     slidesToShow: 3,
     slidesToScroll: 3,
     infinite: true,
     dots: true

     }
     }
     ]
     */
  });

  (function(){
    $(this).bind("contextmenu", function(e) {
      e.preventDefault();
    });
  })();

  $(document).on('click', '[data-reveal-design]', function () {
    var _this= $(this);
    var illustration = _this.context.innerHTML;
    $('#designModal .design-target').empty().append(illustration);
  });

  $(document).on('click', '[data-reveal-illustration]', function () {
    var _this= $(this);
    var illustration = _this.context.innerHTML;
    console.log(illustration);
    if(illustration.indexOf('9') > -1 ){
      console.log('itshim!');
      illustration = illustration.replace('-thumb', '');
    }
    $('#illustrationModal .illustration-target').empty().append(illustration);
  });

  $(document).on('click', '.js-illustration-openfull', function () {
    var imgSrc = $('.illustration-target img').attr('src');
    window.open(imgSrc);
  });

  $(document).on('click', '.js-design-openfull', function () {
    var imgSrc = $('.design-target img').attr('src');
    window.open(imgSrc);
  });

  $(document).on('click', '[data-reveal-interactive]', function () {
    var _this= $(this);
    var source = _this.context.attributes['data-reveal-interactive'].value;
    $('.interactive-modal .js-'+source).css('display', 'block').addClass('active');
  });

  $('.interactive-modal .close-reveal-modal').on('click', function () {
    $('.interactive-modal .active').css('display', 'none');
  });

  $(document).on('close.fndtn.reveal', '[data-reveal]', function () {
    var modal = $(this);
    $(this).find('.active').css('display','none');
  });

  $('[data-navhover]').on({
    mouseenter: function(){
      var target = $(this).data()['navhover'];
      $('.brain-'+target).addClass('show');
    },
    mouseleave: function(){
      var target = $(this).data()['navhover'];
      $('.brain-'+target).removeClass('show');
    }
  });

  /*
      mobile stffsz
  */
  var mobileSectionActive = false;

  $(window).on('resize', function(){
    var win = $(this); //this = window
    if ( (win.innerWidth() > 640) && mobileSectionActive) {
      toggleMobileElements();
    }
  });

  function closeMobileDrawer(){
    $('.off-canvas-wrap').foundation('offcanvas', 'hide', 'move-left');
  }

  $('.js-mobile-drawer-home').on('click', function(){
    closeMobileDrawer();
    if(mobileSectionActive){
      resetMobileContentCtas();
      resetMobileContentSections();
      toggleMobileElements();
      var back = $('.annie-title-back-container');
      if( back.css('display') == 'inline-block'){
        back.css('display', 'none');
      }
    }

  });

  $('.js-mobile-goto-contact').on('click', function(){
    closeMobileDrawer();
    if(mobileSectionActive){
      resetMobileContentCtas();
      resetMobileContentSections();
      toggleMobileElements();
      var back = $('.annie-title-back-container');
      if( back.css('display') == 'inline-block'){
        back.css('display', 'none');
      }
    }

    var target = $('#section_contact')
    $('html, body').animate({
      scrollTop: target.offset().top
    }, 2000);
  });

  $('.js-mobile-goto-contact-fromdrawer').on('click', function(){
    var target = $('#section_contact')
    $('html, body').animate({
      scrollTop: target.offset().top
    }, 2000);
  });


  $('[data-brain-showsection]').on('click', function(){
    // TODO: REMOVE
    return;
    if(window.innerWidth > 640){
      return;
    }
    var whichSection = $(this).data('brain-showsection');
    resetMobileContentCtas();
    resetMobileContentSections();
    hideOtherMobileContentCtas(whichSection);
    showMobileContentSection(whichSection);
  });

  $('[data-goto-mobile-section]').on('click', function(){
    var whichSection = $(this).data('goto-mobile-section');
    closeMobileDrawer();
    if(mobileSectionActive){
      resetMobileContentCtas();
      resetMobileContentSections();
      hideOtherMobileContentCtas(whichSection);
      showMobileContentSection(whichSection);
    }
    else{
      setTimeout(function(){
        flagCtaActive(whichSection);
        hideOtherMobileContentCtas(whichSection);
        showMobileContentSection(whichSection);
        toggleMobileElements();
        toggleMobileNavBack();
      },200);
    }
  });

  $(document).on('click', '.js-mobile-section-cta', function () {
    var _this = $(this);
    if( _this.hasClass('js-active-content-cta') ){
      return;
    }
    scrollToElement(_this);

    var classList = _this.context.classList;
    var whichSection = getMobileContenSectionName(classList);

    flagCtaActive(whichSection);
    hideOtherMobileContentCtas(whichSection);
    showMobileContentSection(whichSection);
    toggleMobileElements();
    toggleMobileNavBack();
  });

  $('.js-nav-back').on('click', function(){
    mobileSectionActive = false;
    $('.js-mobile-section-cta.js-active-content-cta')
      .removeClass('js-active-content-cta');
    toggleMobileElements();
    toggleMobileNavBack();
    resetMobileContentCtas();
    resetMobileContentSections();
    scrollToTop(); 
  });

  function flagCtaActive(whichCta){ 
    $('.js-mobile-section-cta.js-active-content-cta')
      .removeClass('.js-active-content-cta');
    $('.js-mobile-section-cta.mod-'+whichCta).addClass('js-active-content-cta');
    mobileSectionActive = true;
  }


  function hideOtherMobileContentCtas(whichSection){
    $('.js-mobile-section-cta').not('.mod-'+whichSection)
      .css('display','none')
  }

  function resetMobileContentCtas(){
    $('.js-mobile-section-cta').css('display','block');
  }

  function resetMobileContentSections(){
    $('.mobile-content-body.js-active-content-section')
      .removeClass('.js-active-content-cta');
    $('.mobile-content-body')
      .css('display', 'none');
  }

  function showMobileContentSection(whichSection){
    $('.mobile-content-body.mod-'+whichSection)
      .css('display','block')
      .addClass('.js-active-content-section');
  }

  function toggleMobileNavBack(){
    var back = $('.annie-title-back-container');
    if( back.css('display') == 'none'){
      back.css('display', 'inline-block');
    }
    else{
      back.css('display', 'none');
    }
  }

  function getMobileContenSectionName(classList){
    var name = '';
    for(var i=0; i<classList.length; i++){
      if(classList[i].indexOf('mod')>-1){
        name = classList[i];
        break;
      }
    }
    return name.replace('mod-', '');
  }

  function toggleMobileElements(){
    var brain = $('.brain');
    var contact = $('#section_contact');
    if( isBrainVisible() || isContactVisible() ){
      brain.hide();
      contact.hide();
    }
    else{
      brain.show();
      contact.show();
    }
    function isBrainVisible(){
      if( brain.css('display') != 'none' ){
        return true;
      }
      return false;
    }
    function isContactVisible(){
      if( contact.css('display') != 'none' ){
        return true;
      }
      return false;
    }
  }




  /*
      page load functions
  */

  $(window).scroll(function() {

    if($(window).scrollTop() > 24)
    {
    }
    else($(window).scrollTop() <= 23)
    {
    }
  });




});// end document ready

/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*/


function checkThisAnchor(elem, event)
{
  if( elem.is('a') )
  {
      return event.preventDefault();
  }
  else
  {
      return;
  }
}

function get_query()
{
  var url = location.href;
  var qs = url.substring(url.indexOf('?') + 1).split('&');
  for(var i = 0, result = {}; i < qs.length; i++){
    qs[i] = qs[i].split('=');
    result[qs[i][0]] = decodeURIComponent(qs[i][1]);
  }
  return result;
}

function scrollToElement(element)
//element should specify id or class of element
{
  // console.log(element);
  var targetOffset = ($(element).offset().top );
  // var targetOffset = ($(element).offset().top ) - ($(element).outerHeight(true));

  $('html, body').animate({
    scrollTop: targetOffset
  }, 250);
  // console.log(targetOffset );
}

function scrollToTop()
{
  $('html, body').animate({
    scrollTop: $('body').offset().top
  }, 500);
}

function toggleOverlay(target)
// param can be 'container' or 'global'
{
  //print error if target empty
  if(!target)
  {
    console.log('error: toggleOverlay(): no parameter specified');
  }
  else
  {
    var overlay = $('[data-overlay="'+target+'"]');

    if( overlay.is(':visible') )
    {
      overlay.fadeOut(200);
    }
    else{
      overlay.fadeIn(200);
    }
  }
}
